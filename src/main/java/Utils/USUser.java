package Utils;
import java.io.*;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Random;
import java.util.Scanner;
import java.io.*;
import java.util.Properties;
/**
 * Created by Home on 12.10.2017.
 */
public class USUser {
 public  String name;
 public String  country;
 public String city;
  public String phone1;
  public String phone2;
  public String zipcode;
  public String state;

  Properties property = new Properties();
  public static FileInputStream fileInputStream;

  void registrationUS () {

    try {
      fileInputStream = new FileInputStream("src/test/resources/USUser.properties");
      property.load(fileInputStream);
      String name = property.getProperty("name");
      String country = property.getProperty("country");
      String city = property.getProperty("city");
      String phone1 = property.getProperty("phonenumber1");
      String phone2 = property.getProperty("phonenumber2");
      String zipcode = property.getProperty("zipcode");
      String state = property.getProperty("state");
    } catch (IOException e) {
      System.err.println("ОШИБКА: Файл свойств отсуствует!");
    } finally {
      if (fileInputStream != null)
        try {
          fileInputStream.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
}}}