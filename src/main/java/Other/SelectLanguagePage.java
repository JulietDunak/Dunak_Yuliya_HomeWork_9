package Other;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SelectLanguagePage {
    private final WebDriver driver;
    public SelectLanguagePage(WebDriver driver) {
        this.driver = driver;
    }

    @FindBy(xpath = (".//li[contains(@class,'language-pick')]"))
    WebElement languageMenu;

    public void selectLanguage (String line) {

        languageMenu.click();
        driver.findElement(By.id("menu-"+line+"-locale")).click();
            }
}
