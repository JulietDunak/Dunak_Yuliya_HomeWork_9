import Other.OrderProcPage;
import Account.RegistrationPage;
import Utils.USUser;
import Utils.UkrainianUser;
import Utils.MyWaits;
import com.sun.jna.platform.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * Created by Home on 12.10.2017.
 */
public class RegistrationTest {
  static WebDriver driver;
  private static MyWaits wait;
  @BeforeClass
  void setup() {
    System.setProperty("webdriver.chrome.driver", "C:\\Users\\Home\\Downloads\\chromedriver.exe");
    //System.setProperty("webdriver.chrome.driver", "C:\\Users\\it-school\\Desktop\\setup\\chromedriver.exe");
    driver = (WebDriver) new ChromeDriver();
    // driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), DesiredCapabilities.chrome());
    driver.get("https://www.templatemonster.com/");
    driver.manage().window().maximize();
    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    wait = new MyWaits(driver);
  }

  @BeforeClass
    void chekoutTest() {
      OrderProcPage buypr = PageFactory.initElements(driver, OrderProcPage.class);
      buypr.chekout();
    }
  @Test
  void registrationUkrTest(){
    RegistrationPage reg = PageFactory.initElements(driver, RegistrationPage.class);
    UkrainianUser ukrainianUser=new UkrainianUser();
    reg.registerAccount(ukrainianUser.name,ukrainianUser.country, ukrainianUser.city, ukrainianUser.phone1, ukrainianUser.phone2, ukrainianUser.zipcode, ukrainianUser.state);
    Assert.assertTrue(driver.findElement(By.xpath(".//*[@id='payment-methods-container']/div[1]")).isDisplayed());
  }

  @Test
  void registrationUSTest(){
    RegistrationPage reg = PageFactory.initElements(driver, RegistrationPage.class);
    USUser usUser=new USUser();
    reg.registerAccount(usUser.name,usUser.country, usUser.city, usUser.phone1, usUser.phone2, usUser.zipcode, usUser.state);
    Assert.assertTrue(driver.findElement(By.xpath(".//*[@id='payment-methods-container']/div[1]")).isDisplayed());
  }

  public void screenshotFailure(String filename) throws IOException {
    File srcFile=driver.getScreenshotAs(OutputType.FILE);
    File targetFile=new File("./Screenshots/Failure/" + manager.helperBase.generateCurrentDate() + "/" + filename +".jpg");
    FileUtils.copyFile(srcFile,targetFile);
  }
  @AfterClass
  public void tearDown() {
    driver.quit();
  }
  }

